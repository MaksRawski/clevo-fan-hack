#ifndef CLEVO_FAN_CONTROL_H
#define CLEVO_FAN_CONTROL_H

#define EC_SC 0x66
#define EC_DATA 0x62

#define IBF 1
#define OBF 0
#define EC_SC_READ_CMD 0x80

#define EC_REG_SIZE 0x100
#define EC_REG_CPU_TEMP 0x07
#define EC_REG_GPU_TEMP 0xCD
#define EC_REG_FAN_DUTY 0xCE
#define EC_REG_FAN_RPMS_HI 0xD0
#define EC_REG_FAN_RPMS_LO 0xD1

#define MAX_FAN_RPM 4400.0

// Defines maximum speed for which given duty should be used.
// "60, 30" would mean "above 60C the duty should be 30%"
int FAN_CURVE[]={
    0, 0,
    60, 30,
    65, 50,
    70, 80,
    80, 100
};
extern int errno;
#endif // CLEVO_FAN_CONTROL_H
